//
//  ViewController.m
//  AppManager
//
//  Created by Terwer Green on 15/8/8.
//  Copyright (c) 2015年 Terwer Green. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
/**
 *  应用信息
 */
@property (nonatomic,strong) NSArray *apps;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //每列最多显示的数目
    int totalColumns = 3;
    
    //app宽度
    CGFloat appW = 90;
    CGFloat appH = 90;
    
    //app边距
    CGFloat marginX = (self.view.frame.size.width - totalColumns*appW)/(totalColumns +1);
    CGFloat marginY = 10;

    //导航栏高度
    CGFloat navBarH = 44;
    
    //添加控件
    for (int i = 0; i < self.apps.count; i++) {
        
        //计算行号和列号
        int row = i / totalColumns;
        int col = i % totalColumns;
        
        //计算空间的坐标
        CGFloat appX = marginX + (appW + marginX) * col;
        CGFloat appY = navBarH + (appH + marginY) * row;
        
        UIView *appView = [[UIView alloc]init];
        appView.backgroundColor = [UIColor redColor];
        appView.frame = CGRectMake(appX, appY, appW, appH);
        [self.view addSubview:appView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//这里使用了懒加载
-(NSArray *)apps{
    if (_apps == nil) {
        //初始化操作
        //获取plist的全路径
        NSString *path = [[NSBundle mainBundle]pathForResource:@"app.plist" ofType:nil];
        _apps = [NSArray arrayWithContentsOfFile:path];
        //NSLog(@"%@",_apps);
    }
    return _apps;
}

@end
