//
//  ViewController.swift
//  UITableViewDemo
//
//  Created by 唐有炜 on 15/10/23.
//  Copyright © 2015年 Yifa Network. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var tableView:UITableView?
    private var dataSource:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        refreshDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func setupViews (){
        let width = self.view.frame.width
        let height = self.view.frame.height
        tableView = UITableView(frame: CGRectMake(0, 0, width, height),style:.Plain)
        tableView?.dataSource = self
        tableView?.delegate = self
        self.view.addSubview(tableView!)
    }
    
    //UITableVIew dataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell";
        var cell:UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: cellIdentifier)
        }
        cell.textLabel?.text = "测试"
        
        return cell
    }
    
    //UITableView delegate
    
    //refreshDataSource
    func refreshDataSource(){
        for var i in 1...5 {
            dataSource.addObject(NSObject())
        }
        
        self.tableView?.reloadData()
    }
}

