//
//  main.m
//  CommendTest
//
//  Created by 唐有炜 on 15/10/23.
//  Copyright © 2015年 Yifa Network. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Programmer.h"


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        //NSObject *object = [[NSObject alloc] init];
        //NSLog(@"obj:%@ hash:%lx",object,object.hash);
        
        Programmer *p = [[Programmer alloc] init];
        //NSObject *obj = [p self];
        //Class superClass = p.superclass;
        //NSLog(@"%@",superClass.is)
        
        [p performSelector:@selector(privateMethod) withObject:nil];
        
        [p performSelector:@selector(privateMethod:) withObject:@"1"];
        
        [p performSelector:@selector(privateMethod:arg2:) withObject:@"1" withObject:@"2"];
        
        //[p performSelector:@selector(privateMethod) withObject:nil];
    }
    return 0;
}
