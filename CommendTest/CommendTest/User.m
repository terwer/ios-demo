//
//  User.m
//  CommendTest
//
//  Created by 唐有炜 on 15/10/23.
//  Copyright © 2015年 Yifa Network. All rights reserved.
//

#import "User.h"

@implementation User

- (void)privateMethod{
    NSLog(@"我是User里面的私有方法");
}

- (void)privateMethod:(NSString *)arg1{
    NSLog(@"我是User里面的私有方法,一个参数%@",arg1);
}

- (void)privateMethod:(NSString *)arg1 arg2:(NSString *)arg2{
    NSLog(@"我是User里面的私有方法，两个参数,%@ %@",arg1,arg2);
}

- (void)privateMethod:(NSString *)arg1 arg2:(NSString *)arg2 arg3:(NSString *)arg3{
    NSLog(@"我是User里面的私有方法，三个参数%@ %@ %@",arg1,arg2,arg3);
}

@end
