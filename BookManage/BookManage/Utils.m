


//
//  Utils.m
//  BookManage
//
//  Created by szgxa30 on 15/7/23.
//  Copyright (c) 2015年 terwer. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(NSString *)stringFromDate:(NSDate *)date{
    NSDateFormatter *formater1=[[NSDateFormatter alloc]init]; formater1.dateFormat=@"yy-MM-dd HH:mm:ss";
    NSString *dateStr=[formater1 stringFromDate:date];
    return dateStr;
}

@end
