//
//  Utils.h
//  BookManage
//
//  Created by szgxa30 on 15/7/23.
//  Copyright (c) 2015年 terwer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject
+(NSString *)stringFromDate:(NSDate *)date;
@end
