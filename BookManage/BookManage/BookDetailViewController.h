//
//  BookDetailViewController.h
//  BookManage
//
//  Created by szgxa30 on 15/7/23.
//  Copyright (c) 2015年 terwer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface BookDetailViewController : UIViewController

@property NSManagedObject *detail;
//作者
@property (weak, nonatomic) IBOutlet UILabel *labelAuthor;
//图片
@property (weak, nonatomic) IBOutlet UIImageView *image;
//详情
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;

@end
