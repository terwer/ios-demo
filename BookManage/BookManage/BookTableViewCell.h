//
//  BookTableViewCell.h
//  BookManage
//
//  Created by szgxa30 on 15/7/23.
//  Copyright (c) 2015年 terwer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookTableViewCell : UITableViewCell

//书名
@property (weak,nonatomic) IBOutlet UILabel *labelBookName;
//出版社
@property (weak,nonatomic) IBOutlet UILabel *labelBookPublisher;
//作者
@property (weak,nonatomic) IBOutlet UILabel *labelBookAuthor;
//出版日期
@property (weak,nonatomic) IBOutlet UILabel *labelbookPubdate;
//添加日期
@property (weak,nonatomic) IBOutlet UILabel *labelBookAddDate;


@end
