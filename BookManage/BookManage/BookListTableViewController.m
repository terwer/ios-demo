


//
//  BookListTableViewController.m
//  BookManage
//
//  Created by szgxa30 on 15/7/23.
//  Copyright (c) 2015年 terwer. All rights reserved.
//

#import "BookListTableViewController.h"
#import "BookTableViewCell.h"
#import "AppDelegate.h"
#import "Utils.h"
#import "BookDetailViewController.h"

@interface BookListTableViewController (){
    //图书数据集合
    NSArray *books;
}

@end

@implementation BookListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self fecthData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return books.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BookTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BookTableViewCell" forIndexPath:indexPath];
    
    //当前行数据
    NSManagedObject *book = [books objectAtIndex:indexPath.row];
    
    // Configure the cell...
    NSLog(@"books count:%lu",books.count);
    cell.labelBookName.text = [book valueForKey:@"bookName"];
    cell.labelBookAuthor.text = [book valueForKey:@"bookAuthor"];
    cell.labelBookPublisher.text = [book valueForKey:@"bookPublisher"];
    cell.labelbookPubdate.text = [Utils stringFromDate:[book valueForKey:@"bookPubdate"]];
    cell.labelBookAddDate.text = [Utils stringFromDate:[book valueForKey:@"bookAdddate"]];
    
    cell.tag=indexPath.row;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"segueToBookDetail"]) {
        UITableViewCell *cell = sender;
        NSManagedObject *detail = [books objectAtIndex:cell.tag];
        BookDetailViewController *detailController=[segue destinationViewController];
        detailController.detail=detail;
    }
    
}


-(void)fecthData{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    NSManagedObjectModel *model = appDelegate.managedObjectModel;
    NSDictionary *dictionary = [model entitiesByName];
    NSEntityDescription *entity = [dictionary objectForKey:@"Book"];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSPredicate *predicate = [NSPredicate predicateWithValue:YES];
    fetchRequest.entity = entity;
    fetchRequest.predicate = predicate;
    NSError *error;
    books=[NSArray array];
    books = [context executeFetchRequest:fetchRequest error:&error];
   
    NSLog(@"%@",appDelegate.applicationDocumentsDirectory);
}

@end
