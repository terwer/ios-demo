//
//  BookDetailViewController.m
//  BookManage
//
//  Created by szgxa30 on 15/7/23.
//  Copyright (c) 2015年 terwer. All rights reserved.
//

#import "BookDetailViewController.h"

@interface BookDetailViewController ()

@end

@implementation BookDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"%@",[_detail valueForKey:@"bookImage"]);
    
    //self.image=[UIImage imageNamed:[_detail valueForKey:@"bookimage"]];
    self.labelAuthor.text=[_detail valueForKey:@"bookName"];
    self.labelDescription.text=[_detail valueForKey:@"bookDescription"];
    self.labelDescription.numberOfLines=0;
    [self.labelDescription sizeToFit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
