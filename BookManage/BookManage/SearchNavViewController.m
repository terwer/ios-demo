


//
//  SearchNavViewController.m
//  BookManage
//
//  Created by szgxa30 on 15/7/23.
//  Copyright (c) 2015年 terwer. All rights reserved.
//

#import "SearchNavViewController.h"

@interface SearchNavViewController ()

@end

@implementation SearchNavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITableViewController *bookListController=[self.storyboard instantiateViewControllerWithIdentifier:@"BookListTableViewController"];
    
     bookListController.navigationItem.title=NSLocalizedString(@"searchTabbarTitle", nil);
    
    [self pushViewController:bookListController animated:YES];
     [[self tabBarItem] setTitle:NSLocalizedString(@"searchToolbarTitle", nil)];
    [[self tabBarItem] setImage:[UIImage imageNamed:@"search.png"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
