//
//  ViewController.m
//  AppTest
//
//  Created by 唐有炜 on 15/10/23.
//  Copyright © 2015年 Yifa Network. All rights reserved.
//

#import "ViewController.h"
#import "Friend.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self doAction];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)doAction{
    __block Friend *f = [[Friend alloc] init];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [f cleanTimer];
        f = nil;
    });
}

@end
