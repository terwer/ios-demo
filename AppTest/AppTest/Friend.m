//
//  Friend.m
//  AppTest
//
//  Created by 唐有炜 on 15/10/23.
//  Copyright © 2015年 Yifa Network. All rights reserved.
//

#import "Friend.h"

@interface Friend (){
    NSTimer *_timer;
}


@end

@implementation Friend

- (instancetype)init{
    if (self = [super init]) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(handleTimer) userInfo:nil repeats:YES];
        [self handleTimer];
    }
    return self;
}

- (void)handleTimer{
    NSLog(@"我在运行...");
}

- (void)cleanTimer{
    [_timer invalidate];
}

-(void)dealloc{
    //[self cleanTimer];
    NSLog(@"我被销毁了!!!");
}

@end
